#include "HTTPClient.h"
#include "ArduinoJson.h"
#include "mbed.h"
#include "EthernetInterface.h"


#define FS_MIN 0
#define FS_MAX 160

Serial PCSerial (USBTX, USBRX);
DigitalOut myLED(LED1);
// AnalogIn FloodSensor(A0);

EthernetInterface eth;

char URL[] = "http://ingestion-dev.leapcraft.dk/"; // server end-point address
char str[512], str1[512];

int16_t content_size = 0;
// HTTPClient webserver;
uint8_t ret = 0;

int main(int argc, char const *argv[]) {
	// char payload[131];
	PCSerial.baud(115200);


	eth.init();
	eth.connect();
	/* code */

	PCSerial.printf("IP Address is %s\n", eth.getIPAddress());
	PCSerial.printf("Netmask is %s\n", eth.getNetworkMask());
	PCSerial.printf("Gateway is %s\n", eth.getGateway());
	PCSerial.printf("MAC Address is %s\n", eth.getMACAddress());


	while(1)
		{
			wait(5);
			myLED = 1;

			HTTPClient webserver;
			StaticJsonBuffer<500> jsonBuffer;
			JsonObject& root = jsonBuffer.createObject();
			root["app"] = "cphsense";
			root["id"] = "00:02:f7:f0:00:00";
			root["board_id"] = "null";
			root["time"] = "2017-07-12T16:07:00+00:00";
			JsonObject& payload = root.createNestedObject("payload");
			payload["event"]="periodic";
			payload["w_hgt"]=50;
			payload["batt"]=0;
			payload["pm2_5"]=0;
			payload["pm1"]=0;
			payload["pm10"]=0;
			payload["opened"]=1;
			root["type"]="report";

			root.prettyPrintTo(str);
			PCSerial.printf(str);
			PCSerial.printf("Posting to web");
			HTTPText outtext(str);
			HTTPText intext(str1, 512);
			HTTPResult ret = webserver.post("http://ingestion-dev.leapcraft.dk/", outtext, &intext);
			if( ret == HTTP_OK )
		  {
		    PCSerial.printf("POST OK!\n");
		  }
		  else
		  {
		    PCSerial.printf("POST Error %d\n", ret);
		  }

			myLED = 0;


		}

	return 0;
}
